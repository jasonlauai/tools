import sys
from os.path import abspath, join, dirname

sys.path.insert(0, abspath(join(dirname(__file__), "..")))
import json
import random
import geo
from tools.sfm import transformations as tf
from tools.sfm import types
import numpy as np
from tools import extract_exif


def main():
    directory = "/home/me/VisualMap/PoseNet/vivocity_4f_wikicircle/2d/vivocity_4f_clockwise_03"
    image_path_prefix = directory + "/images"
    recon_ref_lla_file = None
    # set None when using original exif gps; set json file produced by reconstruction when employing reconstructed gps
    # recon_ref_lla = None
    recon_ref_lla_file = directory + "/reference_lla.json"

    if recon_ref_lla_file is not None:
        data = json.load(open(recon_ref_lla_file))
        recon_ref_lla = (data['latitude'], data['longitude'], data['altitude'])

    poses = read_reconstruction(directory, recon_ref_lla)
    random.shuffle(poses)
    parse_poses(poses, image_path_prefix)


def parse_poses(poses, image_path_prefix):
    for ratio in [0]:
        length = len(poses)
        train = int(round(length * ratio))
        train_file = open("/home/me/VisualMap/PoseNet/vivocity_3f_wikicircle/2d/dataset_train_15_" + str(ratio) + ".txt", "a")
        test_file = open("/home/me/VisualMap/PoseNet/vivocity_3f_wikicircle/2d/dataset_test_15_" + str(ratio) + ".txt", "a")

        for index in range(train):
            train_file.write(newline(poses[index], image_path_prefix))

        for index in range(train, length):
            test_file.write(newline(poses[index], image_path_prefix))

        train_file.close()
        test_file.close()


def read_reconstruction(directory, recon_ref_lla):
    center_point_ecef = [-2311798.8279636507, 5398385.983429689, 2480359.6352578043]  # vivocity
    # center_point_ecef = [-2311937.919513671, 5398586.003972031, 2479798.3675919706]  # shilong street
    # center_point_ecef = (-2395230.5154681522, 5395508.848729471, 2406859.0426551085)  # hkairport
    # center_point_ecef = [-2417750.805061243, 5386323.90025778, 2404921.744264197]      # k11
    data = json.load(open(directory + "/reconstruction.json"))
    poses = []
    for element in data:
        shots = element["shots"]
        for shotId in shots:
            shot = shots[shotId]
            rotation = shot['rotation']
            translation = shot['translation']
            pose = types.Pose(np.asarray(rotation), np.asarray(translation))
            if recon_ref_lla is None:
                # read gps from original image exif
                origin = extract_exif.ImageMetaData(directory + "/images/" + shotId)
                lat_lng_alt = origin.get_lat_lng_alt()
                ecef = geo.ecef_from_lla(lat_lng_alt[0], lat_lng_alt[1], 15)
                origin = [
                    ecef[0] - center_point_ecef[0],
                    ecef[1] - center_point_ecef[1],
                    ecef[2] - center_point_ecef[2]
                ]
            else:
                # read gps from reconstruction results
                origin = pose.get_origin()
                origin = geo.lla_from_topocentric(origin[0], origin[1], origin[2], recon_ref_lla[0], recon_ref_lla[1],
                                                  recon_ref_lla[2])
                ecef = geo.ecef_from_lla(origin[0], origin[1], origin[2])
                origin = [
                    ecef[0] - center_point_ecef[0],
                    ecef[1] - center_point_ecef[1],
                    ecef[2] - center_point_ecef[2]
                ]
            q = tf.quaternion_from_matrix(pose.get_rotation_matrix())
            poses.append({
                "image_name": shotId,
                "x": origin[0],
                "y": origin[1],
                "z": origin[2],
                "q0": q[0],
                "q1": q[1],
                "q2": q[2],
                "q3": q[3],
            })
    return poses


def newline(pose, image_path_prefix):
    split = " "
    line = image_path_prefix + "/" + pose['image_name'] + split + str(pose['x']) + split + str(
        pose['y']) + split + str(
        pose['z']) \
           + split + str(pose['q0']) + split + str(pose['q1']) + split + str(pose['q2']) + split + str(pose['q3'])
    return line + "\n"


def convert_origin_pose(old_pose, recon_ref_lla, new_ref_lla):
    lla = geo.lla_from_topocentric(old_pose[0], old_pose[1], old_pose[2], recon_ref_lla[0], recon_ref_lla[1],
                                   recon_ref_lla[2])
    xyz = geo.topocentric_from_lla(lla[0], lla[1], lla[2], new_ref_lla[0], new_ref_lla[1], new_ref_lla[2])
    return xyz


if __name__ == '__main__':
    main()
