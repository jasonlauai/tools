import math

import piexif


def degToDmsRational(degFloat):
    minFloat = degFloat % 1 * 60
    secFloat = minFloat % 1 * 60
    deg = math.floor(degFloat)
    min = math.floor(minFloat)
    sec = round(secFloat * 100)
    return ((deg, 1), (min, 1), (sec, 100))


def to_deg(value, loc):
    if value < 0:
        loc_value = loc[0]
    elif value > 0:
        loc_value = loc[1]
    else:
        loc_value = ""
    abs_value = abs(value)
    deg = int(abs_value)
    t1 = (abs_value - deg) * 60
    min = int(t1)
    sec = round((t1 - min) * 60, 5)
    return (deg, min, sec, loc_value)


def view_gps_location(file_name, lat, lng):
    """Adds GPS position as EXIF metadata
    Keyword arguments:
    file_name -- image file
    lat -- latitude (as float)
    lng -- longitude (as float)
    """
    lat_deg = to_deg(lat, ["S", "N"])
    lng_deg = to_deg(lng, ["W", "E"])

    # convert decimal coordinates into degrees, munutes and seconds
    # exiv_lat = (pyexiv2.Rational(lat_deg[0] * 60 + lat_deg[1], 60), pyexiv2.Rational(lat_deg[2] * 100, 6000))
    # exiv_lng = (pyexiv2.Rational(lng_deg[0] * 60 + lng_deg[1], 60), pyexiv2.Rational(lng_deg[2] * 100, 6000))

    # exiv_image = pyexiv2.Image(file_name)
    # exiv_image.readMetadata()
    # exif_keys = exiv_image.exifKeys()

    # for key in exif_keys:
    #     print(key, [exiv_image[key]])

    # print(lat_deg)
    # print(lng_deg)


def set_gps_location(file_name, lat, lng, altitude):
    """Adds GPS position as EXIF metadata
    Keyword arguments:
    file_name -- image file
    lat -- latitude (as float)
    lng -- longitude (as float)
    """

    exif_dict = piexif.load(file_name)
    exif_dict["GPS"][piexif.GPSIFD.GPSLatitudeRef] = 'S' if lat < 0 else 'N'
    exif_dict["GPS"][piexif.GPSIFD.GPSLatitude] = degToDmsRational(lat)
    exif_dict["GPS"][piexif.GPSIFD.GPSLongitudeRef] = 'W' if lng < 0 else 'E'
    exif_dict["GPS"][piexif.GPSIFD.GPSLongitude] = degToDmsRational(lng)
    exif_dict["GPS"][piexif.GPSIFD.GPSMapDatum] = u"WGS-84"
    exif_dict["GPS"][piexif.GPSIFD.GPSVersionID] = (2, 0, 0, 0)
    exif_dict["GPS"][piexif.GPSIFD.GPSAltitudeRef] = 0 if altitude >= 0 else 1
    exif_dict["GPS"][piexif.GPSIFD.GPSAltitude] = (abs(int(altitude*10)), 10)
    exifbytes = piexif.dump(exif_dict)
    piexif.insert(exif=exifbytes, image=file_name)