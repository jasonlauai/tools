import sys
from os.path import abspath, join, dirname

sys.path.insert(0, abspath(join(dirname(__file__), "..")))
import random
import geo
from tools import extract_exif


def main():
    output = "/home/me/VisualMap/PoseNet/hkairport/hkairport_l5_b_04"
    images_path = "/home/me/VisualMap/PoseNet/hkairport/hkairport_l5_b_04/images"
    poses = read_reconstruction(images_path)
    random.shuffle(poses)
    parse_poses(output, poses)


def parse_poses(output, poses):
    for ratio in [0]:
        length = len(poses)
        train = int(round(length * ratio))
        train_file = open(output + "/dataset_train_" + str(ratio) + ".txt", "w")
        test_file = open(output + "/dataset_test_" + str(ratio) + ".txt", "w")
        for index in range(train):
            train_file.write(newline(poses[index]))

        for index in range(train, length):
            test_file.write(newline(poses[index]))

        train_file.close()
        test_file.close()


def read_reconstruction(images_path):
    # center_point_ecef = [-2311798.8279636507, 5398385.983429689, 2480359.6352578043]  # vivocity
    # center_point_ecef = [-2311937.919513671, 5398586.003972031, 2479798.3675919706]  # shilong street
    center_point_ecef = (-2395230.5154681522, 5395508.848729471, 2406859.0426551085)  # hkairport
    # center_point_ecef = [-2417750.805061243, 5386323.90025778, 2404921.744264197]      # k11
    poses = []
    import glob
    shots = glob.glob(images_path + "/*.jpg")
    for shotId in shots:
        origin = extract_exif.ImageMetaData(shotId)
        lat_lng_alt = origin.get_lat_lng_alt()
        origin = [0, 0, 0]
        ecef = geo.ecef_from_lla(lat_lng_alt[0], lat_lng_alt[1], lat_lng_alt[2])
        origin[0] = ecef[0] - center_point_ecef[0]
        origin[1] = ecef[1] - center_point_ecef[1]
        origin[2] = ecef[2] - center_point_ecef[2]

        poses.append({
            "image_name": shotId,
            "x": origin[0],
            "y": origin[1],
            "z": origin[2],
            "q0": 0,
            "q1": 0,
            "q2": 0,
            "q3": 0,
        })
    return poses


def newline(pose):
    split = " "
    line = pose['image_name'] + split + str(pose['x']) + split + str(
        pose['y']) + split + str(
        pose['z']) \
           + split + str(pose['q0']) + split + str(pose['q1']) + split + str(pose['q2']) + split + str(pose['q3'])
    return line + "\n"


if __name__ == '__main__':
    main()
