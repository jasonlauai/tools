import sys
from os.path import abspath, join, dirname

sys.path.insert(0, abspath(join(dirname(__file__), "..")))

import argparse
import json
import os
import random

import cv2
import numpy as np

import geo
from tools import extract_exif
from tools import undistort
from tools.sfm import transformations as tf
from tools.sfm import types

"""
python tools/create_posenet_dataset_undistort.py \
-dir /home/me/VisualMap/PoseNet/vivocity_2f_wikicircle_360/360_9th_circle/0 \
-o /home/me/VisualMap/PoseNet/vivocity_2f_wikicircle_360/360_9th_circle/0 \
-gps True \
-n 6 \
-f 0.5


cat \
/home/me/VisualMap/PoseNet/vivocity_2f_wikicircle_360/360_9th_circle/dataset_train_0.8.txt \
/home/me/VisualMap/PoseNet/vivocity_2f_wikicircle_360/360_11th_circle/dataset_train_0.8.txt \
> /home/me/VisualMap/PoseNet/vivocity_2f_wikicircle_360/i\&o_1circle/dataset_train_0.8.txt

"""


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-dir', dest='dir', required=True, type=str)
    parser.add_argument('-o', dest="output", required=False, type=str, default=".")
    parser.add_argument('-gps', dest="gps_adjustment", required=False, type=bool, default=False)
    parser.add_argument('-ht', dest="floor_height", required=False, type=float, default=0.0)
    parser.add_argument('-n', dest="angle_number", required=False, type=int, default=6)
    parser.add_argument('-f', dest="focal_setting", required=False, type=float, default=0.5)
    return parser.parse_args()


def main():
    # directory = "/Users/zhongzhiming/Locision_space/bee-view/scene-reconstruction/data/test/"
    args = parse_args()
    directory = args.dir
    output = args.output
    gps_adjustment = args.gps_adjustment
    floor_height = args.floor_height
    angle_num = args.angle_number
    focal = args.focal_setting

    if gps_adjustment:
        old_ref_lla_file = directory + "reference_lla.json"
        data = json.load(open(old_ref_lla_file))
        # old_ref_lla = (data['latitude'], data['longitude'], data['altitude'])
        old_ref_lla = (data['latitude'], data['longitude'], floor_height-2)
        # floor height minus 2 to reduce the height set for camera position
    else:
        old_ref_lla = None
    poses = read_reconstruction(directory, old_ref_lla, output, floor_height, angle_num, focal)
    random.shuffle(poses)
    parse_poses(output, poses, angle_num, focal)


def parse_poses(output, poses, angle_num, focal):
    for ratio in [1.0]:
        length = len(poses)
        train = int(round(length * ratio))
        train_file = open(output + "/dataset_train_" + str(focal) + "_" + str(int(360/angle_num)) + "_" + str(ratio) + ".txt", "w")
        test_file = open(output + "/dataset_test_" + str(focal) + "_" + str(int(360/angle_num)) + "_" + str(ratio) + ".txt", "w")

        for index in range(train):
            train_file.write(newline(poses[index]))

        for index in range(train, length):
            test_file.write(newline(poses[index]))

        train_file.close()
        test_file.close()


def read_reconstruction(directory, old_ref_lla, output, floor_height, angle_num, focal):
    center_point_ecef = [-2311798.8279636507, 5398385.983429689, 2480359.6352578043]  # vivocity
    # center_point_ecef = [-2416529.4022947694, 5387420.836538212, 2403705.478842201]  # IFC
    data = json.load(open(directory + "reconstruction.meshed.json"))
    poses = []
    for element in data:
        shots = element["shots"]
        for shotId in shots:
            # if shotId not in line_list:
            #     continue
            shot = shots[shotId]
            rotation = shot['rotation']
            translation = shot['translation']
            pose = types.Pose(np.asarray(rotation), np.asarray(translation))
            image_path = directory + "/images/" + shotId

            if old_ref_lla is None:
                # read gps from original image exif
                origin = extract_exif.ImageMetaData(image_path)
                lat_lng_alt = origin.get_lat_lng_alt()
                ecef = geo.ecef_from_lla(lat_lng_alt[0], lat_lng_alt[1], floor_height)
                origin = [
                    ecef[0] - center_point_ecef[0],
                    ecef[1] - center_point_ecef[1],
                    ecef[2] - center_point_ecef[2]
                ]
            else:
                # read gps from reconstruction results
                origin = pose.get_origin()
                origin = geo.lla_from_topocentric(origin[0], origin[1], origin[2], old_ref_lla[0], old_ref_lla[1],
                                                  old_ref_lla[2])
                ecef = geo.ecef_from_lla(origin[0], origin[1], origin[2])
                origin = [
                    ecef[0] - center_point_ecef[0],
                    ecef[1] - center_point_ecef[1],
                    ecef[2] - center_point_ecef[2]
                ]

            subshots = undistort.perspective_views_of_a_panorama(pose, angle_num=angle_num)
            undistorts = undistort.undistort_image(image_path, pose, subshots, focal=focal)
            filename = os.path.basename(image_path)
            for index in range(len(subshots)):
                file_name = filename.replace(".jpg", "") + "_" + str(index) + ".jpg"
                dir_ = output + "F" + str(focal) + "_" + str(int(360/angle_num)) + "/"
                if not os.path.isdir(dir_):
                    os.makedirs(dir_)
                cv2.imwrite(dir_ + file_name, undistorts[index][:, :, ::-1])
                print("undistort images:%s, save at:%s" % (shotId, dir_ + file_name))
                q = tf.quaternion_from_matrix(subshots[index].get_rotation_matrix())
                poses.append({
                    "image_name": dir_ + file_name,
                    "x": origin[0],
                    "y": origin[1],
                    "z": origin[2],
                    "q0": q[0],
                    "q1": q[1],
                    "q2": q[2],
                    "q3": q[3],
                })
    return poses


def newline(pose):
    split = " "
    line = pose['image_name'] + split + str(pose['x']) + split + str(
        pose['y']) + split + str(pose['z']) + \
           split + str(pose['q0']) + split + str(pose['q1']) + split + str(pose['q2']) + split + str(pose['q3'])
    return line + "\n"


if __name__ == '__main__':
    main()
