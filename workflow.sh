#!/usr/bin/env bash
##!/usr/bin/env bash


## undistort panorama images with various focal and angle number settings
# ATEENTION! should set correct center_point_ecef
# ATEENTION! should set correct altitude
#
#f="4f"
#ht=300.0
#
#for dir in $(ls /media/me/VisualMap/PoseNet/vivocity_foshan_d3fmv9/$f/ | grep -E '^[0-9]$')
#do
#    d=$(echo /media/me/VisualMap/PoseNet/vivocity_foshan_d3fmv9/${f}/${dir}/)
#    echo ${d}
#    python tools/create_posenet_dataset_undistort.py -dir ${d} -o ${d} -gps True -ht $ht -n 18 -f 0.5
#    python tools/create_posenet_dataset_undistort.py -dir ${d} -o ${d} -gps True -ht $ht -n 12 -f 1.0
#    python tools/create_posenet_dataset_undistort.py -dir ${d} -o ${d} -gps True -ht $ht -n 18 -f 1.3
#done
#
#mkdir -p /media/me/VisualMap/PoseNet/vivocity_foshan_d3fmv9/$f/
#echo "create /media/me/VisualMap/PoseNet/vivocity_foshan_d3fmv9/$f/"
#
#ds=`echo /media/me/VisualMap/PoseNet/vivocity_foshan_d3fmv9/$f/*/*.txt`
#txt_dir=/media/me/VisualMap/PoseNet/vivocity_foshan_d3fmv9/$f/train_fh${ht}_1.0.txt
#cat $ds > $txt_dir
#echo "${#ds[*]} files were merged to $txt_dir"

mkdir -p /media/me/VisualMap/PoseNet/vivocity_foshan_d3fmv9/all_floors/fh100/train
txt=`echo /media/me/VisualMap/PoseNet/vivocity_foshan_d3fmv9/*/train_fh*_1.0.txt`
cat $txt > /media/me/VisualMap/PoseNet/vivocity_foshan_d3fmv9/all_floors/train_fh100_1.0.txt


### start training with tensorboard mornitoring
#tensorboard --logdir 1circle:/media/me/VisualMap/PoseNet/vivocity_2f_wikicircle_360/multi_gpu_pretrain_360_1circle_9_11/F0.5_10_1.0_30_1.3_20/,all_floors:/media/me/VisualMap/PoseNet/vivocity_foshan_d3fmv9/all_floors
##
python multi_gpu_train.py \
-i 200000 \
-f /media/me/VisualMap/PoseNet/vivocity_foshan_d3fmv9/all_floors/train_fh100_1.0.txt \
-o /media/me/VisualMap/PoseNet/vivocity_foshan_d3fmv9/all_floors/fh100_2fc/train \
-r /media/me/VisualMap/PoseNet/vivocity_foshan_d3fmv9/all_floors/train/model.ckpt-200000

