import argparse
import sys
from os.path import abspath, join, dirname
from pathlib import Path

sys.path.insert(0, abspath(join(dirname(__file__), "..")))
import os
import cv2
import numpy as np

from tools.sfm import extract_exif
from tools.sfm import transformations as tf
from tools.sfm import types
from tools.sfm import write_gps

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', dest='json_file', required=True, type=str)
    return parser.parse_args()

def undistort_image(image_path, spherical_pose, undistorted_shots, focal, subshot_width, camera_num):
    original = imread(image_path)
    width = 4 * subshot_width
    height = width // 2
    image = cv2.resize(original, (width, height), interpolation=cv2.INTER_AREA)
    undistort_images = []
    for subshot in undistorted_shots:
        undistorted = render_perspective_view_of_a_panorama(image, spherical_pose, subshot, focal, camera_num)
        undistort_images.append(undistorted)
        # data.save_undistorted_image(subshot.id, undistorted)
    return undistort_images

def imread(filename):
    """Load image as an RGB array ignoring EXIF orientation."""
    flags = cv2.IMREAD_COLOR | cv2.IMREAD_IGNORE_ORIENTATION
    bgr = cv2.imread(filename, flags)
    return bgr[:, :, ::-1]  # Turn BGR to RGB

def perspective_views_of_a_panorama(spherical_pose, angle_num):
    """Create 6 perspective views of a panorama."""

    # names = ['front', 'left', 'back', 'right', 'top', 'bottom']
    # names = ['front', 'left', 'back', 'right']
    angle = 2 * np.pi / angle_num
    num = int(np.pi * 2 / angle)

    rotations = []
    for i in range(num):
        rotations.append(tf.rotation_matrix(i * angle, (0, 1, 0)))

    poses = []
    for rotation in rotations:
        R = np.dot(rotation[:3, :3], spherical_pose.get_rotation_matrix())
        o = spherical_pose.get_origin()
        pose = types.Pose()
        pose.set_rotation_matrix(R)
        pose.set_origin(o)
        poses.append(pose)

    return poses

def render_perspective_view_of_a_panorama(image, panoshot_pose, perspectiveshot_pose, focal, camera_size):
    """Render a perspective view of a panorama."""
    # Get destination pixel coordinates

    # camera_height = camera_width = 256
    camera_height = camera_width = camera_size
    dst_shape = (camera_height, camera_width)
    dst_y, dst_x = np.indices(dst_shape).astype(np.float32)
    dst_pixels_denormalized = np.column_stack([dst_x.ravel(), dst_y.ravel()])

    dst_pixels = normalized_image_coordinates(dst_pixels_denormalized, camera_width, camera_height)

    # Convert to bearing
    dst_bearings = perspectiveshot_pixel_bearing_many(dst_pixels, focal)

    # Rotate to panorama reference frame
    rotation = np.dot(panoshot_pose.get_rotation_matrix(),
                      perspectiveshot_pose.get_rotation_matrix().T)
    rotated_bearings = np.dot(dst_bearings, rotation.T)

    # Project to panorama pixels
    src_x, src_y = panoshot_project((rotated_bearings[:, 0],
                                     rotated_bearings[:, 1],
                                     rotated_bearings[:, 2]))
    src_pixels = np.column_stack([src_x.ravel(), src_y.ravel()])

    src_pixels_denormalized = denormalized_image_coordinates(
        src_pixels, image.shape[1], image.shape[0])

    src_pixels_denormalized.shape = dst_shape + (2,)

    # Sample color
    x = src_pixels_denormalized[..., 0].astype(np.float32)
    y = src_pixels_denormalized[..., 1].astype(np.float32)
    colors = cv2.remap(image, x, y, cv2.INTER_LINEAR)

    return colors

def perspectiveshot_pixel_bearing_many(pixels, focal):
    k = np.array([[focal, 0., 0.], [0., focal, 0.], [0., 0., 1.]])
    k1 = k2 = 0.0
    """Unit vectors pointing to the pixel viewing directions."""
    points = pixels.reshape((-1, 1, 2)).astype(np.float64)
    distortion = np.array([k1, k2, 0., 0.])
    up = cv2.undistortPoints(points, k, distortion)
    up = up.reshape((-1, 2))
    x = up[:, 0]
    y = up[:, 1]
    l = np.sqrt(x * x + y * y + 1.0)
    return np.column_stack((x / l, y / l, 1.0 / l))

def panoshot_project(point):
    """Project a 3D point in camera coordinates to the image plane."""
    x, y, z = point
    lon = np.arctan2(x, z)
    lat = np.arctan2(-y, np.sqrt(x ** 2 + z ** 2))
    return np.array([lon / (2 * np.pi), -lat / (2 * np.pi)])

def normalized_image_coordinates(pixel_coords, width, height):
    size = max(width, height)
    p = np.empty((len(pixel_coords), 2))
    p[:, 0] = (pixel_coords[:, 0] + 0.5 - width / 2.0) / size
    p[:, 1] = (pixel_coords[:, 1] + 0.5 - height / 2.0) / size
    return p

def denormalized_image_coordinates(norm_coords, width, height):
    size = max(width, height)
    p = np.empty((len(norm_coords), 2))
    p[:, 0] = norm_coords[:, 0] * size - 0.5 + width / 2.0
    p[:, 1] = norm_coords[:, 1] * size - 0.5 + height / 2.0
    return p

def main(read_path,save_path,subshot_width,camera_size,focal,angle_num):
    # rotation = [1.2847309868840149, -0.8909395928771724, 1.0583504053031378],
    # translation = [0.06178642615907228, 2.0086067356239066, -0.17751037095586553]
    rotation = [0,0,0],
    translation = [0,0,0]
    image_path = read_path
    pose = types.Pose(np.asarray(rotation), np.asarray(translation))
    subshots = perspective_views_of_a_panorama(pose,angle_num)
    undistorts = undistort_image(image_path, pose, subshots, focal, subshot_width, camera_size)

    filename = os.path.basename(image_path)
    dirname = os.path.dirname(image_path)
    names = ['front', 'left', 'back', 'right']
    meta_data = extract_exif.ImageMetaData(image_path)
    lat_lng = meta_data.get_lat_lng()

    for index in range(len(undistorts)):
        # file = dirname + "/" + filename.replace(".jpg", "") + "_" + names[index] + ".jpg"
        # cv2.imwrite(file, undistorts[index][:, :, ::-1])
        file = save_path + '/' + read_path.split('/')[-1].replace('.jpg','_') + names[index] + '.jpg'
        cv2.imwrite(file,undistorts[index][:, :, ::-1])
        # write_gps.set_gps_location(file, lat_lng[0], lat_lng[1], 2.0)

if __name__ == '__main__':
    p_read = Path('/Users/mapxus/jason/python/objectdetection/Data/batch2/') # <- updated
    img = list(p_read.glob('*.jpg'))
    angle_num = 2
    img = img[:20] # try first 20 image
    for subshot_width in [320,640,960,1280]:
        for camera_size in [256,512,768,1024]:
            for focal in [0.1,0.2,0.3,0.4,0.5]:
                p_save = Path('/Users/mapxus/jason/python/objectdetection/Data/batch2_{}_{}_{}_{}/'.format(angle_num,focal,subshot_width,camera_size)) # <- update
                os.mkdir(p_save)
                for idx, file_ in enumerate(img):
                    img_1 = str(file_)
                    print(img_1.split('/')[-1])
                    main(read_path=img_1,save_path=str(p_save),subshot_width=subshot_width,camera_size=camera_size,focal=focal,angle_num=angle_num)
